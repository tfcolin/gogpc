module gitee.com/tfcolin/gogpc

go 1.17

require (
	gitee.com/tfcolin/dsg v0.0.0-20220314100148-ee03f8eb65e2
	gitee.com/tfcolin/gomat v0.0.0-20220309094909-fe425858bfd0
)
