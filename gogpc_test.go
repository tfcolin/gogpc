package gogpc

import (
	"fmt"
	"testing"
)

func TestGPC(t *testing.T) {

	p1 := InitPoly()

	p1.NewBound(false)
	p1.NewPoint(-1, -1)
	p1.NewPoint(0, -1)
	p1.NewPoint(1, 0)
	p1.NewPoint(-1, 0)
	p1.NewBound(false)
	p1.NewPoint(-0.5, -0.6)
	p1.NewPoint(-0.5, -0.3)
	p1.NewPoint(0, -0.3)
	p1.NewPoint(0, -0.6)
	p1.NewBound(false)
	p1.NewPoint(0, -3)
	p1.NewPoint(1, -3)
	p1.NewPoint(2, -2)
	p1.NewPoint(1.5, -1.5)
	p1.NewPoint(2, -1)
	p1.NewPoint(1, -1)
	p1.NewPoint(1, -2)
	p1.NewPoint(0, -2)

	p1.Finish()

	p2 := InitPoly()

	p2.NewBound(false)
	p2.NewPoint(0.5, -2.5)
	p2.NewPoint(3, -2.5)
	p2.NewPoint(3, 1)
	p2.NewPoint(0.5, 1)
	p2.NewBound(false)
	p2.NewPoint(1, -1.5)
	p2.NewPoint(1.5, -1.5)
	p2.NewPoint(1.5, -0.5)
	p2.NewPoint(1, -0.5)
	p2.NewBound(false)
	p2.NewPoint(1.5, -2)
	p2.NewPoint(2, -1.5)
	p2.NewPoint(2, -1)
	p2.NewPoint(1.75, -0.5)
	p2.NewPoint(2.5, -0.5)
	p2.NewPoint(2.5, -2)

	p2.Finish()

	p1.SetPoly(0)
	p2.SetPoly(1)

	PolyDiff(0, 1, 2)
	PolyInter(0, 1, 3)
	PolyUnion(0, 1, 4)

	p3 := GetPoly(2)
	p4 := GetPoly(3)
	p5 := GetPoly(4)

	pmin, pmax := p1.CalBoundingBox()
	fmt.Println("BB1: ", pmin, pmax)
	pmin, pmax = p2.CalBoundingBox()
	fmt.Println("BB2: ", pmin, pmax)
	pmin, pmax = p3.CalBoundingBox()
	fmt.Println("BB3: ", pmin, pmax)
	pmin, pmax = p4.CalBoundingBox()
	fmt.Println("BB4: ", pmin, pmax)
	pmin, pmax = p5.CalBoundingBox()
	fmt.Println("BB5: ", pmin, pmax)

	d1 := InitAsyDrawer("p1.asy")
	d2 := InitAsyDrawer("p2.asy")
	d3 := InitAsyDrawer("p3.asy")
	d4 := InitAsyDrawer("p4.asy")
	d5 := InitAsyDrawer("p5.asy")

	p1.Draw(d1, 0)
	p2.Draw(d2, 0)
	p3.Draw(d3, 0)
	p4.Draw(d4, 0)
	p5.Draw(d5, 0)

}

func make_proj (z Point, x Point) []float64 {
	y := Point{0, 0, 0}
	CrossProd(z, x, y)
	Unify(x)
	Unify(y)
	Unify(z)
	proj := append(x, y...)
	proj = append(proj, z...)

	return proj
}

func TestFaceProj(t *testing.T) {

		z := Point{2, -0.9, 0.3}
		x := Point{0.9, 2, 0}

	/*
		z := Point{0.9, -2, -0.2}
		x := Point{2, 0.9, 0}
	*/

	/*
		z := Point{-1, 1.3, -0.1}
		x := Point{-1.32, -1, 0.2}
	*/

	/*
		z := Point{0, 0, 1}
		x := Point{1, 0, 0}
	*/

	/*
	z := Point{0, -1, -0.3}
	x := Point{1, 0, 0}
	*/

	proj := make_proj (z, x)

	s := ReadScene("scene2.dat")
	d := InitAsyDrawer ("scene2.asy")
	s.Draw (proj, d, []int{0, 1, 2, 3})
}

func TestProjCheck (t *testing.T) {

	z := Point{2, -0.9, 0.3}
	x := Point{0.9, 2, 0}
	proj_dir := Point{-1.2, 1, 0.2}

	proj := make_proj (z, x)

	s := ReadScene("scene2.dat")
	wlist := ReadScene("win2.dat")
	win := wlist.area[0]

	d := InitAsyDrawer ("scene2_proj.asy")

	res, shadow := s.CheckProjection (proj_dir, win, true)
	s.Draw (proj, d, []int{0, 1, 2, 3})
	shadow.Draw (proj, d, []int{4, 4, 5, 5})

	fmt.Printf ("On projection = %t\n", res)
}

