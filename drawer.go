package gogpc

import "fmt"
import "os"

type AsyDrawer struct {
	fout * os.File 
}

func InitAsyDrawer (fout_name string) * AsyDrawer {
	var d AsyDrawer 
	d.fout,_ = os.Create (fout_name)
	fmt.Fprintln(d.fout, "size (15cm, true);")
	return &d
}

func (d * AsyDrawer) Draw (p1, p2 []float64, class int) {
	var pstr string

	switch class {
		case 0: 
		pstr = "solid"
	case 1:
		pstr = "dashed"
	case 2:
		pstr = "blue"
	case 3:
		pstr = "dashed + blue"
	case 4:
		pstr = "brown"
	case 5:
		pstr = "yellow"
	case 6:
		pstr = "heavygreen"
	case 7:
		pstr = "magenta"
	default:
		pstr = "nullpen"
	}

	fmt.Fprintf (d.fout, "draw ((%12.4f, %12.4f) -- (%12.4f, %12.4f), p = %s);\n", 
	p1[0], p1[1], p2[0], p2[1], pstr)

}
