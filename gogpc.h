#include "gpc.h"

void GPCInit ();
void GPCFinalize ();

void SetNewPoly (int id);
void SetNewBound (int npoint);
void SetNewPoint (double x, double y);
void SetEndBound ();
void SetEndPoly ();

int    GetNBound (int id);
char   GetIsHole (int id, int ibound);
int    GetNPoint (int id, int ibound);
double GetPointX (int id, int ibound, int iv);
double GetPointY (int id, int ibound, int iv);

void PolyDiff (int p1, int p2, int res);
void PolyUnion (int p1, int p2, int res);
void PolyInter (int p1, int p2, int res);

